package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func main() {
	var (
		exts         = flag.String("ext", ".java", "Comma-separated list of file extensions to parse (e.g., '.java,.go,.txt')")
		excludeDirs  = flag.String("exclude-dirs", "", "Comma-separated list of directories to exclude (e.g., 'dir1,dir2')")
		excludeExts  = flag.String("exclude-exts", "", "Comma-separated list of file extensions to exclude (e.g., '.log,.tmp')")
		excludeFiles = flag.String("exclude-files", "", "Comma-separated list of file names to exclude (e.g., 'file1,file2')")
		outputRoot   = flag.String("output-root", "", "Root directory to prepend to file paths in the output")
		renameFile   = flag.String("rename", "Snippet", "Word to use instead of 'File'")
		formats      = flag.String("formats", "md", "Comma-separated list of output formats (e.g., 'md,txt')")
		help         = flag.Bool("help", false, "Show usage information")
	)
	flag.Parse()

	if *help || flag.NArg() < 1 {
		printUsage()
		return
	}

	extList := strings.Split(*exts, ",")
	excludeDirList := strings.Split(*excludeDirs, ",")
	excludeExtList := strings.Split(*excludeExts, ",")
	excludeFileList := strings.Split(*excludeFiles, ",")
	outputFormatList := strings.Split(*formats, ",")

	dir := flag.Arg(0)
	outputFilename := "parse"
	if len(outputFormatList) > 0 {
		outputFilename += "." + outputFormatList[0]
	}

	if flag.NArg() > 1 {
		outputFilename = flag.Arg(1)
	}

	if err := processDirectory(dir, outputFilename, outputFormatList, extList, excludeDirList, excludeExtList, excludeFileList, *outputRoot, *renameFile); err != nil {
		log.Fatal(err)
	}
}

func processDirectory(dir, outputFilename string, outputFormats, exts, excludeDirs, excludeExts, excludeFiles []string, outputRoot, renameFile string) error {
	outputFiles := make(map[string]*os.File)
	defer func() {
		for _, file := range outputFiles {
			file.Close()
		}
	}()

	for _, format := range outputFormats {
		filename := outputFilename
		if len(outputFormats) > 1 {
			ext := filepath.Ext(outputFilename)
			name := strings.TrimSuffix(outputFilename, ext)
			filename = fmt.Sprintf("%s_%s%s", name, format, ext)
		}
		file, err := os.Create(filename)
		if err != nil {
			return err
		}
		outputFiles[format] = file
	}

	return filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			for _, excludeDir := range excludeDirs {
				if filepath.Base(path) == excludeDir {
					return filepath.SkipDir
				}
			}
			return nil
		}

		ext := filepath.Ext(path)
		if !contains(exts, ext) {
			return nil
		}

		for _, excludeExt := range excludeExts {
			if ext == excludeExt {
				return nil
			}
		}

		for _, excludeFile := range excludeFiles {
			if filepath.Base(path) == excludeFile {
				return nil
			}
		}

		for format, outputFile := range outputFiles {
			if err := processFile(path, outputFile, format, outputRoot, ext, renameFile); err != nil {
				return err
			}
		}
		return nil
	})
}

func processFile(filePath string, output io.Writer, format, outputRoot, extension, renameFile string) error {
	relPath, err := filepath.Rel(outputRoot, filePath)
	if err != nil {
		relPath = filePath
	}

	data, err := os.ReadFile(filePath)
	if err != nil {
		return err
	}

	extension, _ = strings.CutPrefix(extension, ".")

	switch format {
	case "md":
		fmt.Fprintf(output, "%s `%s`\n```%s\n%s\n```\n", renameFile, relPath, extension, data)
	case "txt":
		fmt.Fprintf(output, "%s %s\n%s\n", renameFile, relPath, data)
	}

	return nil
}

func contains(list []string, item string) bool {
	for _, v := range list {
		if v == item {
			return true
		}
	}
	return false
}

func printUsage() {
	fmt.Println("Usage: codeparser [flags] <input_directory> [output_file]")
	fmt.Println("Flags:")
	fmt.Println("  --ext string\t\tComma-separated list of file extensions to parse (e.g., '.java,.go,.txt')")
	fmt.Println("  --exclude-dirs string\tComma-separated list of directories to exclude (e.g., 'dir1,dir2')")
	fmt.Println("  --exclude-exts string\tComma-separated list of file extensions to exclude (e.g., '.log,.tmp')")
	fmt.Println("  --exclude-files string\tComma-separated list of file names to exclude (e.g., 'file1,file2')")
	fmt.Println("  --output-root string\tRoot directory to prepend to file paths in the output")
	fmt.Println("  --rename string\t\tWord to use instead of 'File' (e.g., 'Snippet')")
	fmt.Println("  --formats string\t\tComma-separated list of output formats (e.g., 'md,txt')")
	fmt.Println("  --help\t\tShow usage information and exit")
	fmt.Println("Example:")
	fmt.Println("  codeparser -ext .go,.txt -exclude-dirs build,temp -exclude-exts .log -exclude-files temp.txt -output-root ./source -rename Snippet -formats md,txt ./source ./output")
}
