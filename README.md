# Code Parser

A simple code parser that allows you to parse the code of your project in a single .md or .txt file.

## Installation

```
go build codeparser.go
```

## Usage

```
Usage: codeparser [flags] <input_directory> [output_file]
Flags:
  --txt    Use plain text format for output (Markdown by default)
  --ext string  File extension to parse (e.g., '.java', '.go', '.cs')
  --help   Show usage information and exit
Example:
  codeparser --txt --ext .go ./source .

```